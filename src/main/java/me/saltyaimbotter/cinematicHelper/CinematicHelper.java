package me.saltyaimbotter.cinematicHelper;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.HashMap;
import java.util.UUID;

public final class CinematicHelper extends JavaPlugin {
    public static HashMap<UUID, ItemStack[]> playerInventories = new HashMap<>();
    public static Plugin plugin;
    public static FileConfiguration config;
    public static File configFile;

    public static Plugin getPlugin() {
        return plugin;
    }

    public static void reloadDataStorage() {
        plugin.reloadConfig();
    }

    @Override
    public void onEnable() {
        this.plugin = this;
        config = this.getConfig();
        configFile = new File(this.getDataFolder(), "config.yml");
        getCommand("startCinematic").setExecutor(new CinematicCommand());
        getServer().getPluginManager().registerEvents(new InventoryProtector(), this);
        saveDefaultConfig();
    }

    @Override
    public void onDisable() {
        for (UUID u : playerInventories.keySet()) {
            Bukkit.getPlayer(u).getInventory().setContents(playerInventories.get(u));
        }
    }
}
