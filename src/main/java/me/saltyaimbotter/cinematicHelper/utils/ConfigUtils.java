package me.saltyaimbotter.cinematicHelper.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;
import java.util.ArrayList;

import static me.saltyaimbotter.cinematicHelper.CinematicHelper.*;

public class ConfigUtils {

    public static ConfigurationSection createOrGetConfigAddress(String playerUUIDString) {
        if (config.isSet(playerUUIDString)) {
            return config.getConfigurationSection(playerUUIDString);
        } else {
            return config.createSection(playerUUIDString);
        }
    }

    public static void safeConfig() {
        try {
            config.save(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveLocationToConfig(String playerUUID, Location loc) {
        ConfigurationSection playerData = createOrGetConfigAddress(playerUUID);
        playerData.set("world", loc.getWorld().getName());
        playerData.set("x", loc.getX());
        playerData.set("y", loc.getY());
        playerData.set("z", loc.getZ());
        playerData.set("pitch", loc.getPitch());
        playerData.set("yaw", loc.getYaw());

        safeConfig();
    }

    public static Location getLocationFromConfig(String playerUUID) {
        reloadDataStorage();
        ConfigurationSection playerData = config.getConfigurationSection(playerUUID);
        World destinationWorld = Bukkit.getWorld(playerData.getString("world"));
        double x = playerData.getInt("x");
        double y = playerData.getDouble("y");
        double z = playerData.getDouble("z");
        float yaw = (float) playerData.getDouble("yaw");
        float pitch = (float) playerData.getDouble("pitch");

        safeConfig();
        return new Location(destinationWorld, x, y, z, pitch, yaw);
    }

    public static void saveInventoryToConfig(String playerUUID, ItemStack[] inventoryContents) {
        ConfigurationSection playerData = createOrGetConfigAddress(playerUUID);
        playerData.set("inv", inventoryContents);
        safeConfig();
    }

    public static ItemStack[] getInventoryFromConfig(String playerUUID) {
        Object x = config.get(playerUUID + ".inv");
        ArrayList<ItemStack> xc =  (ArrayList<ItemStack>) x;
        ItemStack[] pre = new ItemStack[39];
        return xc.toArray(pre);
    }
}
