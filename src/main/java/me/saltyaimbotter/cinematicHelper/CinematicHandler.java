package me.saltyaimbotter.cinematicHelper;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

import static me.saltyaimbotter.cinematicHelper.CinematicHelper.config;
import static me.saltyaimbotter.cinematicHelper.InventoryProtector.*;
import static me.saltyaimbotter.cinematicHelper.ItemCreator.createBlackBars;
import static me.saltyaimbotter.cinematicHelper.utils.ConfigUtils.*;

public class CinematicHandler {


    public static HashMap<UUID, Integer[]> taskIDs = new HashMap<>();
    public static HashMap<UUID, Location> safeLocations = new HashMap<>();

    public static void runCinematic(Player player, Integer duration, Location cinematiclocation, Location endLocation) {
        saveInventory(player);
        saveInventoryToConfig(player.getUniqueId().toString(), player.getInventory().getContents());
        saveLocationToConfig(player.getUniqueId().toString(), player.getLocation());
        clearInventory(player);
        for (int i = 0; i < 9; i++) {
            player.getInventory().setItem(i, ItemCreator.createBlackOutItem());
        }

        player.getInventory().setItem(5, createBlackBars());


        int TaskIDTP = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(CinematicHelper.getPlugin(), () -> {
            player.teleport(cinematiclocation);
            player.getInventory().setHeldItemSlot(5);
        }, 0, 1);

        int TaskIDEnd = Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(CinematicHelper.getPlugin(), () -> {
            Bukkit.getServer().getScheduler().cancelTask(TaskIDTP);
            clearInventory(player);
            restoreInventory(player);
            player.teleport(endLocation);

            config.set(player.getUniqueId().toString(), null);
            safeConfig();

        }, duration);
        taskIDs.put(player.getUniqueId(), new Integer[]{TaskIDTP, TaskIDEnd});
        safeLocations.put(player.getUniqueId(), endLocation);
    }


}
