package me.saltyaimbotter.cinematicHelper;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import static me.saltyaimbotter.cinematicHelper.CinematicHandler.safeLocations;
import static me.saltyaimbotter.cinematicHelper.CinematicHelper.*;
import static me.saltyaimbotter.cinematicHelper.utils.ConfigUtils.*;

public class InventoryProtector implements Listener {

    public static void saveInventory(Player player) {
        ItemStack[] inventoryContents = player.getInventory().getContents().clone();
        playerInventories.put(player.getUniqueId(), inventoryContents);
    }

    public static void clearInventory(Player player) {
        player.setItemOnCursor(null);
        player.getInventory().clear();
    }

    public static void restoreInventory(Player player) {
        player.getInventory().setContents(playerInventories.get(player.getUniqueId()));
        playerInventories.remove(player.getUniqueId());

    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent event) {
        if (!playerInventories.containsKey(event.getPlayer().getUniqueId())) {
            return;
        }
        event.setCancelled(true);
    }

    @SuppressWarnings("deprecation")
    @EventHandler
    public void onPickup(PlayerPickupItemEvent event) {
        if (!playerInventories.containsKey(event.getPlayer().getUniqueId())) {
            return;
        }
        event.setCancelled(true);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (!playerInventories.containsKey(event.getWhoClicked().getUniqueId())) {
            return;
        }
        event.setCancelled(true);
    }

    @EventHandler
    public void onDrag(InventoryDragEvent event) {
        if (!playerInventories.containsKey(event.getWhoClicked().getUniqueId())) {
            return;
        }
        event.setCancelled(true);
    }


    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        Player p = event.getPlayer();
        if (!playerInventories.containsKey(p.getUniqueId())) {
            return;
        }
        clearInventory(p);
        restoreInventory(p);
        p.teleport(safeLocations.get(p.getUniqueId()));

        config.set(p.getUniqueId().toString(), null);
        getPlugin().saveConfig();

        Integer[] taskIDs = CinematicHandler.taskIDs.get(p.getUniqueId());
        Bukkit.getScheduler().cancelTask(taskIDs[0]);
        Bukkit.getScheduler().cancelTask(taskIDs[1]);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        if (config.isSet(event.getPlayer().getUniqueId().toString())) {
            Player p = event.getPlayer();
            String uuid = p.getUniqueId().toString();

            p.teleport(getLocationFromConfig(uuid));
            p.getInventory().setContents(getInventoryFromConfig(uuid));

            config.set(uuid, null);
            safeConfig();
        }
    }

}
