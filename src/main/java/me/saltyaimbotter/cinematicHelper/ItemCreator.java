package me.saltyaimbotter.cinematicHelper;

import net.minecraft.server.v1_12_R1.*;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemCreator {


    public static ItemStack createBlackBars() {
        ItemStack blackBarsItem = NMSHelper(new ItemStack(Material.GOLD_HOE));
        blackBarsItem.setDurability((short) 30);
        ItemMeta meta = blackBarsItem.getItemMeta();
        meta.setDisplayName(" ");
        blackBarsItem.setItemMeta(meta);
        return blackBarsItem;
    }

    public static ItemStack NMSHelper(ItemStack item) {
        net.minecraft.server.v1_12_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound compound = (nmsStack.hasTag()) ? nmsStack.getTag() : new NBTTagCompound();
        NBTTagList modifiers = new NBTTagList();

        NBTTagCompound speed = new NBTTagCompound();
        speed.set("AttributeName", new NBTTagString("generic.attackSpeed"));
        speed.set("Name", new NBTTagString("generic.attackSpeed"));
        speed.set("Amount", new NBTTagDouble(Double.MAX_VALUE));
        speed.set("Operation", new NBTTagInt(0));
        speed.set("UUIDLeast", new NBTTagInt(894654));
        speed.set("UUIDMost", new NBTTagInt(2272));
        speed.set("Slot", new NBTTagString("mainhand"));

        modifiers.add(speed);
        compound.set("AttributeModifiers", modifiers);
        nmsStack.setTag(compound);
        return CraftItemStack.asBukkitCopy(nmsStack);
    }

    static ItemStack createBlackOutItem() {
        short durabiltyDamage = 32;
        ItemStack item = new ItemStack(Material.GOLD_HOE);
        item.setDurability(durabiltyDamage);
        item.getItemMeta().setUnbreakable(true);

        return item;
    }
}
