package me.saltyaimbotter.cinematicHelper;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.saltyaimbotter.cinematicHelper.CinematicHandler.runCinematic;

public class CinematicCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "This command can only be executed by a player!");
            return true;
        }
        if (args.length != 11) {
            sender.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.WHITE + "Please inform the admins that there is an error in this quest!" + ChatColor.DARK_RED + " [Not enough arguments!]");
            return true;
        }
        Player player = (Player) sender;
        if (CinematicHelper.playerInventories.containsKey(player.getUniqueId())) {
            sender.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.WHITE + "Please inform the admins that there is an error in this quest!" + ChatColor.DARK_RED + " [Command is called multiple times!]");
            return true;
        }
        runCinematic(player, Integer.parseInt(args[0]), new Location(player.getWorld(), Double.parseDouble(args[1]), Double.parseDouble(args[2]), Double.parseDouble(args[3]), Float.parseFloat(args[4]), Float.parseFloat(args[5])), new Location(player.getWorld(), Double.parseDouble(args[6]), Double.parseDouble(args[7]), Double.parseDouble(args[8]), Float.parseFloat(args[9]), Float.parseFloat(args[10])));

        return true;
    }
}
